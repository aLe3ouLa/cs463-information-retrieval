/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SearchQuery;

/**
 *
 * @author Eirini
 */
public class Weight {
   private double value; 
   
   public Weight(){
       this.value = 0;
   }
    
   public double increment(double n){
       double v = getValue();
       return v + n;
   }
   
   public double decrement(double n){
       double v = getValue();
       return v - n;
   }
   
   public double getValue(){
       return value;
   }
   
   public void setValue(double value){
       this.value = value;
   }
            
    
}
