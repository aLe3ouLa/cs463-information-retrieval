package SearchQuery;

import IndexFiles.DocumentRef;
import static IndexFiles.IndexFiles.createHashStopWords;
import static IndexFiles.IndexFiles.isInStopWordsEN;
import static IndexFiles.IndexFiles.isInStopWordsGR;
import static IndexFiles.IndexFiles.removePunctuation;
import gr.uoc.csd.hy463.Topic;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;
import mitos.stemmer.Stemmer;

public class SearchFiles {

    private static final HashSet<String> HASH_STOPWORDS = new HashSet<>();

    HashMap<String, Integer> freqInQuery = new HashMap<>();
    int MAXRESULTS = 1000;
    HashMap<String, QueryRetrieval> result = new HashMap<>();
    HashMap<String, Weight> queryWeight = new HashMap<>();
    List<QueryRetrieval> finalRes = new ArrayList<>();

    public boolean isNumeric(String s) {
        return s.matches("[-+]?\\d*\\.?\\d+");
    }

    public static void createStopwordHash() throws FileNotFoundException, IOException {
        createHashStopWords("./stopwordsGr.txt", HASH_STOPWORDS);
        createHashStopWords("./stopwordsEn.txt", HASH_STOPWORDS);
    }

    public static String queryOperations(String query) throws FileNotFoundException, IOException {
        String newQuery = removePunctuation(query);
        newQuery = newQuery.toLowerCase();

        if (!isInStopWordsGR(HASH_STOPWORDS, newQuery) && !isInStopWordsEN(HASH_STOPWORDS, newQuery)) {
            newQuery = Stemmer.Stem(newQuery);
            return newQuery;
        }
        return null;
    }

    private void IndexQuery(String query, String delimiter) throws IOException {
        StringTokenizer tokenizer = new StringTokenizer(query, delimiter);
        while (tokenizer.hasMoreTokens()) {
            String tokenToIndex = tokenizer.nextToken();
            String newQuery = queryOperations(tokenToIndex);

            if (newQuery != null) {
                if (freqInQuery.containsKey(newQuery)) {
                    int val = freqInQuery.get(newQuery) + 1;
                    freqInQuery.put(newQuery, val);
                } else {
                    freqInQuery.put(newQuery, 1);
                }

            }
        }
    }

    private QueryRetrieval CalculateScore(double queryLength, DocumentRef docRef, double score) {
        double finalScore = 0;
        if (docRef.getFileNormal() != 0 && queryLength != 0) {
            finalScore = (score) / (sqrt(queryLength) * docRef.getFileNormal());
        }

        QueryRetrieval queryRetrieval = new QueryRetrieval(docRef, finalScore);
        return queryRetrieval;
    }

    private Weight CalculateWeightQueryToken(String query, double count, double max, double idf) {
        Weight weightQuery = new Weight();
        double tfq = count / max;
//        if(query.contains("diagnosis") || query.contains("test") || query.contains("treatment"))
//           weightQuery.setValue(idf * tfq);
//        else
        weightQuery.setValue(idf * tfq * 1.1);

        queryWeight.put(query, weightQuery);
        return weightQuery;
    }

    private String[] getLineVoc(StringTokenizer lineTokens) {
        String[] currTok = new String[3];
        int tokenIndex = 0;
        while (lineTokens.hasMoreTokens()) {
            currTok[tokenIndex] = lineTokens.nextToken();
            tokenIndex++;
        }
        return currTok;
    }

    private String[] getLinePosting(StringTokenizer lineTokens) {
        String[] currTok = new String[4];
        int tokenIndex = 0;
        while (lineTokens.hasMoreTokens()) {
            currTok[tokenIndex] = lineTokens.nextToken();
            tokenIndex++;
        }
        return currTok;
    }

    private String[] getLineDocument(StringTokenizer lineTokens) {
        String[] currTok = new String[3];
        int tokenIndex = 0;
        while (lineTokens.hasMoreTokens()) {
            currTok[tokenIndex] = lineTokens.nextToken();
            tokenIndex++;
        }
        return currTok;
    }

    public SearchFiles(String query, Topic topic) throws FileNotFoundException, IOException, Exception {
        String delimiter = "\t\n\r\f ";
        RandomAccessFile DocumentsFile = new RandomAccessFile("CollectionIndex/DocumentsFile.txt", "rw");
        RandomAccessFile PostingFile = new RandomAccessFile("CollectionIndex/PostingFile.txt", "rw");

        IndexQuery(query, delimiter);
        float max = Collections.max(freqInQuery.entrySet(), (entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).getValue();

        String thisLine = null;
        int HowManyWordsReadVoc = 0;
        double QueryLength = 0;
        BufferedReader vocabulary = new BufferedReader(new InputStreamReader(new FileInputStream("CollectionIndex/VocabularyFile.txt"), "UTF-8")); //diavazei th grammh apo to vocabulary 
        while ((thisLine = vocabulary.readLine()) != null) {

            String[] currTok = getLineVoc(new StringTokenizer(thisLine, " "));
            currTok[0] = currTok[0].replaceAll("\\W", "");
            //System.out.println("Current Voc word: " + currTok[0] + " " + currTok[1] + " " + currTok[2]);

            //ama uparxei sto hashmap h le3h tou vocabulary
            if (freqInQuery.containsKey(currTok[0])) {
                if (HowManyWordsReadVoc == freqInQuery.size()) {
                    break;
                }
                vocabulary.mark(1000); //mrkare to voc wste na diabaseis sto posting file oti afora to termi
                String[] nextLineTokens = getLineVoc(new StringTokenizer(vocabulary.readLine(), " ")); //diabase thn epomenh le3h tou voc

                if (currTok[2] != null) {
                    if (nextLineTokens[2] != null) {
                        // to current kai to next den einai null shmainei oti dne einai h teleutaia le3h tou voc

                        //diabase oles tis grammes pou endiaferoun
                        for (long i = Long.parseLong(currTok[2]); i < Long.parseLong(nextLineTokens[2]) - 2;) {
                            String[] PostingFileArgs;
                            String[] DocFileArgs;

                            long whereToSeek = i;
                            PostingFile.seek(whereToSeek);

                            PostingFileArgs = getLinePosting(new StringTokenizer(PostingFile.readLine(), " "));
                            long pointer = PostingFile.getFilePointer();

                            long whereToSeekFile = Long.parseLong(PostingFileArgs[3]);

                            //ypologise to weight tou query + th norma tou query
                            Weight w = CalculateWeightQueryToken(currTok[0], freqInQuery.get(currTok[0]), max, Double.parseDouble(PostingFileArgs[2]));
                            QueryLength += pow(w.getValue(), 2);

                            // upologismos tou weight tou eggrafou me bash to posting file
                            double w_i_j = Double.parseDouble(PostingFileArgs[1]) * Double.parseDouble(PostingFileArgs[2]);

                            DocumentsFile.seek(whereToSeekFile); // seek sto document file

                            DocFileArgs = getLineDocument(new StringTokenizer(DocumentsFile.readLine(), " "));

                            double documentLength = Float.parseFloat(DocFileArgs[2]);

                            DocumentRef docRef = new DocumentRef(new File(DocFileArgs[1]), documentLength);
                            QueryRetrieval qr = new QueryRetrieval(docRef, w_i_j * w.getValue());
                       //     System.out.println(currTok[0] + " " + qr.getDocRef().getFile().getName());
                            
                            if (result.containsKey(docRef.getFile().getName())) {
                                QueryRetrieval hs = result.get(docRef.getFile().getName());
                                hs.setScore(hs.getScore() + qr.getScore());
                            } else {
                                result.put(docRef.getFile().getName(), qr);
                            }
                            i = pointer;
                        }
                    }
                } else {
                    //last word in vocabulary 
                    for (long i = Long.parseLong(currTok[2]); i < PostingFile.length();) {
                        String[] PostingFileArgs;
                        String[] DocFileArgs;

                        long whereToSeek = i;
                        PostingFile.seek(whereToSeek);

                        PostingFileArgs = getLinePosting(new StringTokenizer(PostingFile.readLine(), " "));
                        long pointer = PostingFile.getFilePointer();
                        long whereToSeekFile = Long.parseLong(PostingFileArgs[3]);

                        Weight w = CalculateWeightQueryToken(currTok[0], freqInQuery.get(currTok[0]), max, Float.parseFloat(PostingFileArgs[2]));
                        QueryLength += pow(w.getValue(), 2);

                        double w_i_j = Float.parseFloat(PostingFileArgs[1]) * Float.parseFloat(PostingFileArgs[2]);

                        DocumentsFile.seek(whereToSeekFile);

                        DocFileArgs = getLineDocument(new StringTokenizer(DocumentsFile.readLine(), " "));

                        double documentLength = Float.parseFloat(DocFileArgs[2]);
                        DocumentRef docRef = new DocumentRef(new File(DocFileArgs[1]), documentLength);

                        QueryRetrieval qr = new QueryRetrieval(docRef, w_i_j * w.getValue());
                            
                        if (result.containsKey(docRef.getFile().getName())) {
                                QueryRetrieval hs = result.get(docRef.getFile().getName());
                                hs.setScore(hs.getScore() + qr.getScore());
                            } else {
                                result.put(docRef.getFile().getName(), qr);
                            }
                        i = pointer;
                    }

                }
                HowManyWordsReadVoc++;
                vocabulary.reset();
            }
        }

        for (String file : result.keySet()) {
            QueryRetrieval hs = result.get(file);
                QueryRetrieval docFinalScore = CalculateScore(QueryLength, hs.getDocRef(), hs.getScore());
                if (topic != null) {
                    docFinalScore.setTopicNo(topic.getNumber());
                }
                if (docFinalScore.getScore() != 0.0) {
                    finalRes.add(docFinalScore);
                }

            
        }

        Collections.sort(finalRes, (QueryRetrieval o2, QueryRetrieval o1) -> o1.compareTo(o2));

        List<QueryRetrieval> temp = new ArrayList<>();

        for (int i = 0; i < finalRes.size(); i++) {
            if (i < MAXRESULTS) {
                temp.add(finalRes.get(i));
            } else {
                break;
            }
        }

        finalRes = null;
        finalRes = temp;

    }

    public List<QueryRetrieval> getPassToFrontEnd() {
        return finalRes;
    }

    public HashMap<String, HashMap<String, String>> getPassVocToFrontEnd() {
        // return vocabularyFound;
        return null;
    }
}
