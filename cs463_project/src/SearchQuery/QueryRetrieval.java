package SearchQuery;

import IndexFiles.DocumentRef;

public class QueryRetrieval {
    private int topicNo;
    private DocumentRef docRef;
    private double score;
    
    public QueryRetrieval(DocumentRef docRef, double score){
        this.docRef = docRef;
        this.score = score;
    }
    

    /**
     * @return the docRef
     */
    public DocumentRef getDocRef() {
        return docRef;
    }

    /**
     * @param docRef the docRef to set
     */
    public void setDocRef(DocumentRef docRef) {
        this.docRef = docRef;
    }

    /**
     * @return the score
     */
    public double getScore() {
        return score;
    }

    /**
     * @param score the score to set
     */
    public void setScore(double score) {
        this.score = score;
    }
    
    public int compareTo(QueryRetrieval qr) {
        
        return qr.getScore() > this.getScore()? -1:1;
    }

    /**
     * @return the topicNo
     */
    public int getTopicNo() {
        return topicNo;
    }

    /**
     * @param topicNo the topicNo to set
     */
    public void setTopicNo(int topicNo) {
        this.topicNo = topicNo;
    }
    
    
}
