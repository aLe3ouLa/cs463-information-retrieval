package SystemEvaluation;


public class EvaluationProperties {
    
    private int topicNo;
    private String PMCID;
    private int relevance;
    private double score;
    
    public EvaluationProperties(int topicNo, String PMCID , int relevance){
        this.topicNo = topicNo;
        this.PMCID = PMCID;
        this.relevance = relevance;
    }
    
    public EvaluationProperties(int topicNo, String PMCID , double score){
        this.topicNo = topicNo;
        this.PMCID = PMCID;
        this.score = score;
    }

    /**
     * @return the topicNo
     */
    public int getTopicNo() {
        return topicNo;
    }

    /**
     * @param topicNo the topicNo to set
     */
    public void setTopicNo(int topicNo) {
        this.topicNo = topicNo;
    }
    
     public double getScore() {
        return topicNo;
    }

    /**
     * @param topicNo the topicNo to set
     */
    public void setScore(double score) {
        this.score = score;
    }

    /**
     * @return the PMCID
     */
    public String getPMCID() {
        return PMCID;
    }

    /**
     * @param PMCID the PMCID to set
     */
    public void setPMCID(String PMCID) {
        this.PMCID = PMCID;
    }

    /**
     * @return the relevance
     */
    public int getRelevance() {
        return relevance;
    }

    /**
     * @param relevance the relevance to set
     */
    public void setRelevance(int relevance) {
        this.relevance = relevance;
    }


    
}
