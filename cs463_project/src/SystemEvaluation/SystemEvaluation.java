package SystemEvaluation;

import IndexFiles.FrontEnd;
import SearchQuery.QueryRetrieval;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import static java.lang.Math.E;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class SystemEvaluation {

    private static HashMap<Integer, List<EvaluationProperties>> MetricsOfQrels;
    private static HashMap<Integer, List<EvaluationProperties>> MetricsOfResults;

    public static double bprefCalculation(List<EvaluationProperties> resultTopics, List<EvaluationProperties> qrelsTopics, double R) {
        double summation = 0;
        int n = 0;

        for (int i = 0; i < resultTopics.size(); i++) {
            for (int j = 0; j < qrelsTopics.size(); j++) {
                if (qrelsTopics.get(j).getPMCID().equals(resultTopics.get(i).getPMCID())) {

                    if (qrelsTopics.get(j).getRelevance() > 0) {
                        //System.out.println("i am relevant! ");
                        summation += (1 - (double) (Math.abs(n) / (double) R));
                    } else {
                        n++;
                    }
                }
            }
        }

        return (double) summation / (double) R;
    }

    public static void main(String[] args) throws IOException {
        String delimiter = "\t\n\r\f ";
        MetricsOfQrels = new HashMap<>();
        MetricsOfResults = new HashMap<>();

        String thisLineQrels = null;
        String thisLineResults = null;
        BufferedReader results = new BufferedReader(new InputStreamReader(new FileInputStream("CollectionIndex/results.txt"), "UTF-8")); //diavazei th grammh apo to vocabulary 
        BufferedReader qrels = new BufferedReader(new InputStreamReader(new FileInputStream("qrels.txt"), "UTF-8")); //diavazei th grammh apo to vocabulary 
        
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("./CollectionIndex/eval_results.txt"));
        HashMap <Integer, Double[]> topicsToMetrics = new HashMap<>();
                
        HashMap<Integer, Integer[]> topicRelevance = new HashMap<>();
        while ((thisLineQrels = qrels.readLine()) != null) {
            List<EvaluationProperties> list = new ArrayList<>();
            String[] thingies = new String[4];
            int index = 0;
            // System.out.println(thisLineQrels);
            StringTokenizer tokenizer = new StringTokenizer(thisLineQrels, delimiter);
            while (tokenizer.hasMoreTokens()) { //oso uparxoun kai alla tokens
                String tokenToIndex = tokenizer.nextToken();
                thingies[index] = tokenToIndex;
                index++;
            }

            EvaluationProperties ep = new EvaluationProperties(Integer.parseInt(thingies[0]), thingies[2], Integer.parseInt(thingies[3]));

            int topicNo = Integer.parseInt(thingies[0]);

            if (MetricsOfQrels.containsKey(topicNo)) {
                List<EvaluationProperties> e = MetricsOfQrels.get(topicNo);
                e.add(ep);
                MetricsOfQrels.put(topicNo, e);

                Integer[] temp = topicRelevance.get(topicNo);
                if (ep.getRelevance() > 0) {
                    temp[1] += 1;
                } else {
                    temp[0] += 1;
                }
            } else {
                Integer[] relNR = new Integer[2];
                list.add(ep);
                MetricsOfQrels.put(topicNo, list);
                if (ep.getRelevance() > 0) {
                    relNR[0] = 0;
                    relNR[1] = 1;
                } else {
                    relNR[0] = 1;
                    relNR[1] = 0;
                }
                topicRelevance.put(topicNo, relNR);
            }
        }

        while ((thisLineResults = results.readLine()) != null) {
            List<EvaluationProperties> reslist = new ArrayList<>();
            String[] thingies = new String[6];
            int index = 0;
            StringTokenizer tokenizer = new StringTokenizer(thisLineResults, delimiter);
            while (tokenizer.hasMoreTokens()) { //oso uparxoun kai alla tokens
                String tokenToIndex = tokenizer.nextToken();
                thingies[index] = tokenToIndex;
                index++;
            }

            EvaluationProperties epRes = new EvaluationProperties(Integer.parseInt(thingies[0]), thingies[2], Double.parseDouble(thingies[4]));

            int topicNo = Integer.parseInt(thingies[0]);

            if (MetricsOfResults.containsKey(topicNo)) {
                List<EvaluationProperties> eRes = MetricsOfResults.get(topicNo);
                eRes.add(epRes);
                MetricsOfResults.put(topicNo, eRes);
            } else {
                reslist.add(epRes);
                MetricsOfResults.put(topicNo, reslist);
            }
        }

        //--bpref 
        for (Integer obj : MetricsOfResults.keySet()) {
            Integer[] b = topicRelevance.get(obj);
            double bpref = bprefCalculation(MetricsOfResults.get(obj), MetricsOfQrels.get(obj), b[1]);
            Double [] board = new Double[3];
            board[0]= bpref;
            topicsToMetrics.put(obj,board );
            System.out.println("Topic: " + obj + " bpref = " + bpref);
        }

        HashMap<Integer, List<EvaluationProperties>> RelevantOnlyResults = new HashMap<>();
        for (Integer obj : MetricsOfResults.keySet()) {
            List<EvaluationProperties> list = new ArrayList<>();
            for (int i = 0; i < MetricsOfResults.get(obj).size(); i++) {
                for (int j = 0; j < MetricsOfQrels.get(obj).size(); j++) {
                    if (MetricsOfQrels.get(obj).get(j).getPMCID().equals(MetricsOfResults.get(obj).get(i).getPMCID())) {
                        MetricsOfResults.get(obj).get(i).setRelevance(MetricsOfQrels.get(obj).get(j).getRelevance());
                        list.add(MetricsOfResults.get(obj).get(i));
                    }
                }
            }
            RelevantOnlyResults.put(obj, list);
        }

        for (Integer obj : RelevantOnlyResults.keySet()) {
            Integer[] b = topicRelevance.get(obj);
            List<EvaluationProperties> listRel = RelevantOnlyResults.get(obj);
            double sum = 0;
            int count = 0;
            int rel = 0;
            for (int i = 0; i < listRel.size(); i++) {
                if (listRel.get(i).getRelevance() > 0) {
                    rel++;
                }
                count++;
                sum += RelevantOnlyResults.get(obj).get(i).getRelevance() * ((double) rel / (double) count);
            }
            double avep = sum / b[1]; //sum/R
            
            Double [] board = topicsToMetrics.get(obj);
            board[1]= avep;
            

            System.out.println("Topic: " + obj + " avep = " + avep);

        }

        for (Integer obj : RelevantOnlyResults.keySet()) {
           
            List<EvaluationProperties> listRel = RelevantOnlyResults.get(obj);
            double dcg = 0;
            List<Integer> idcg = new ArrayList<>();
            if (listRel.size() > 0) {
                dcg = listRel.get(0).getRelevance();
                if (listRel.get(0).getRelevance() > 0) {
                    idcg.add(listRel.get(0).getRelevance());
                }
                for (int i = 1; i < listRel.size(); i++) {
                    if (listRel.get(i).getRelevance() > 0) {
                        idcg.add(listRel.get(i).getRelevance());
                        dcg += (double) listRel.get(i).getRelevance() / (double)(Math.log(i + 1) / Math.log(2));
                    }
                }
            }
            
            double idcg1  =0 ;
            
            Collections.sort(idcg);
            Collections.reverse(idcg);

            if (!idcg.isEmpty()) {
               idcg1 = idcg.get(0);
                for (int r = 1; r < idcg.size(); r++) {
                    idcg1 += (double)idcg.get(r) / (double)(Math.log(r + 1) / Math.log(2));
                }
            }

            double ndcg = 0;
            if (idcg1 != 0) {
                ndcg = (double) dcg / (double) idcg1; //sum/R 
            }
            
            Double [] board = topicsToMetrics.get(obj);
            board[2]= ndcg;
            System.out.println("Topic: " + obj + " ndcg = " + ndcg);

            bufferedWriter.write(obj+ "\t"+ board[0] +"\t"+ board[1]+"\t"+board[2]+"\n");
        }
        
        bufferedWriter.close();
    }
}
