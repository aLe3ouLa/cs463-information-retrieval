package IndexFiles;

import static IndexFiles.IndexFiles.*;
import gr.uoc.csd.hy463.NXMLFileReader;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import mitos.stemmer.Stemmer;

public class MainClass {

    private static final HashSet<String> HASH_STOPWORDS = new HashSet<>();

    private final Map<String, Token> tokenHash;
    private final HashMap<String, File> fileList;
    private final Map<File, Double> treeMapDoc;
    private final HashMap<DocumentRef, Integer> maxFreq;

    ArrayList<String> tagList;

    private static void createStopwordHash() throws FileNotFoundException, IOException {
        createHashStopWords("./stopwordsGr.txt", HASH_STOPWORDS);
        createHashStopWords("./stopwordsEn.txt", HASH_STOPWORDS);
    }

    private static String fileName(String file) {
        file = file.replace(".nxml", "");
        return file;
    }

    private void indexDocuments(String FolderName) throws IOException {
        File folder = new File(FolderName);
        listFilesForFolder(folder, fileList);
        createStopwordHash();
        Stemmer.Initialize();
        System.out.println("Docs:" + fileList.size());

        int i = 0;
        for (String file : fileList.keySet()) {
            FileDocument doc = new FileDocument(fileList.get(file));
            IndexDocument(doc);
            i++;
            System.out.println(i);
        }
        computeIDFandDocumentLengths();
    }

    private void IndexDocument(FileDocument document) throws IOException {
        tagList = new ArrayList<>();
        String delimiter = "\t\n\r\f ";
        NXMLFileReader xmlFile = new NXMLFileReader(document.getFile());
        DocumentRef docRef = new DocumentRef(document);
        String pmcid = xmlFile.getPMCID();
        String title = xmlFile.getTitle();
        String abstr = xmlFile.getAbstr();
        String body = xmlFile.getBody();
        String journal = xmlFile.getJournal();
        String publisher = xmlFile.getPublisher();
        ArrayList<String> authors = xmlFile.getAuthors();
        HashSet<String> categories = xmlFile.getCategories();
        int maxFreqForThisDoc = 1;

        tagList.clear();
        tagList.add(pmcid);
        tagList.add(title);
        tagList.add(abstr);
        tagList.add(body);
        tagList.add(journal);
        tagList.add(publisher);
        String authorsList = authors.toString();
        String categoriesList = categories.toString();
        tagList.add(authorsList);
        tagList.add(categoriesList);

        HashMap<String, Integer> tokenToFreq = new HashMap<>();
        for (int i = 0; i < tagList.size(); i++) {
            StringTokenizer tokenizer = new StringTokenizer(tagList.get(i), delimiter);
            while (tokenizer.hasMoreTokens()) {
                String tokenToIndex = tokenizer.nextToken();
                String currentToken = removePunctuation(tokenToIndex);
                currentToken = currentToken.replaceAll("[\\W]|_", "");
                currentToken = currentToken.toLowerCase();
                
                if (i!=0){
                    currentToken = currentToken.replaceAll("\\d",""); 
                }
                
                if (!isInStopWordsGR(HASH_STOPWORDS, currentToken) && !isInStopWordsEN(HASH_STOPWORDS, currentToken)) {
                    currentToken = Stemmer.Stem(currentToken);
                    if ("".equals(currentToken)) {
                        continue;
                    }

                    if (tokenToFreq.containsKey(currentToken)) {
                        int val = tokenToFreq.get(currentToken) + 1;
                        tokenToFreq.put(currentToken, val);
                        if (val > maxFreqForThisDoc) {
                            maxFreqForThisDoc = val;
                        }
                    } else {
                        tokenToFreq.put(currentToken, 1);
                    }
                }
            }
        }
        for (String tok : tokenToFreq.keySet()) {
            IndexToken(tok, tokenToFreq.get(tok), docRef);
        }

        maxFreq.put(docRef, maxFreqForThisDoc);

    }

    private void IndexToken(String Token, int Freq, DocumentRef docRef) {
        TokenOccurence Occ = new TokenOccurence(Freq, docRef);
        if (tokenHash.containsKey(Token)) {
            Token curr = tokenHash.get(Token);
            //List<TokenOccurence> currListOcc = curr.getOccurenceList();
            // currListOcc.add(Occ);
            curr.setOccurenceList(Occ);
            // tokenHash.put(Token, curr);
        } else {
            Token token = new Token();
            token.getOccurenceList().add(Occ);
            tokenHash.put(Token, token);
        }

    }

    private void computeIDFandDocumentLengths() {
        for (String token : tokenHash.keySet()) {
            List<TokenOccurence> to = tokenHash.get(token).getOccurenceList();
            int dfi = to.size();
            double idf = Math.log((double) fileList.size() / (double) dfi) / Math.log(2);
            tokenHash.get(token).setIdf(idf);
            for (int i = 0; i < to.size(); i++) {

                if (tagList.get(3).contains(token)) {
                    to.get(i).getDocRef().setFileNormal(to.get(i).getDocRef().getFileNormal() + Math.pow((idf * (to.get(i).getFrequency() / (double) maxFreq.get(to.get(i).getDocRef()))* 1.5), 2));
                } else if (tagList.get(2).contains(token)) {
                    to.get(i).getDocRef().setFileNormal(to.get(i).getDocRef().getFileNormal() + Math.pow((idf * (to.get(i).getFrequency() / (double) maxFreq.get(to.get(i).getDocRef()))* 1.5), 2));
                } else if (tagList.get(1).contains(token)) {
                   to.get(i).getDocRef().setFileNormal(to.get(i).getDocRef().getFileNormal() + Math.pow((idf * (to.get(i).getFrequency() / (double) maxFreq.get(to.get(i).getDocRef()))* 1.5), 2));
                } else {
                    to.get(i).getDocRef().setFileNormal(to.get(i).getDocRef().getFileNormal() + Math.pow((idf * (to.get(i).getFrequency() / (double) maxFreq.get(to.get(i).getDocRef())) ), 2));
                }

                treeMapDoc.put(to.get(i).getDocRef().getFile(), to.get(i).getDocRef().getFileNormal());

            }
        }

    }

    public int size() {
        return tokenHash.size();
    }

    public void clear() {
        tokenHash.clear();
    }

    public void print() {
        for (Object tokens : tokenHash.keySet()) {
            System.out.print(tokens + ": ");
            List<TokenOccurence> tokOcc = tokenHash.get(tokens).getOccurenceList();
            for (int i = 0; i < tokOcc.size(); i++) {
                TokenOccurence to = tokOcc.get(i);
                System.out.print(to.getDocRef().getFile().getName() + " ");
            }
            System.out.println();
        }
    }

    public MainClass(String foldername) throws FileNotFoundException, IOException {

        File outPut = new File("./CollectionIndex");
        outPut.mkdir();
        RandomAccessFile vocabularyWriter = new RandomAccessFile("CollectionIndex/VocabularyFile.txt", "rw");
        RandomAccessFile docWriter = new RandomAccessFile("CollectionIndex/DocumentsFile.txt", "rw");
        RandomAccessFile postingFileWriter = new RandomAccessFile("CollectionIndex/PostingFile.txt", "rw");
        long whereTheHellISeeTheTerm = 0;
        HashMap<String, Long> whereSeen = new HashMap<>();

        vocabularyWriter.setLength(0);
        docWriter.setLength(0);
        postingFileWriter.setLength(0);

        this.maxFreq = new HashMap<>();
        this.fileList = new HashMap<>();
        this.tokenHash = new TreeMap<>();
        this.treeMapDoc = new TreeMap<>();

        indexDocuments(foldername);

        long whereTheHellISeeTheDoc = 0;
        for (File keyDoc : treeMapDoc.keySet()) {
            docWriter.write(StandardCharsets.UTF_8.encode(fileName(keyDoc.getName()).replaceAll("\\W", "") + " " + keyDoc.getAbsolutePath() + " " + Math.sqrt(treeMapDoc.get(keyDoc)) + "\n").array());
            whereSeen.put(keyDoc.getName(), whereTheHellISeeTheDoc);
            whereTheHellISeeTheDoc = docWriter.getFilePointer();
        }
        docWriter.close();
        System.out.println("DONE WRITE DOCUMENT FILE! ");

        for (String key : tokenHash.keySet()) {
            vocabularyWriter.write(StandardCharsets.UTF_8.encode(key.replaceAll("\\W", "") + " " + tokenHash.get(key).getOccurenceList().size() + " " + whereTheHellISeeTheTerm + "\n").array());
            for (TokenOccurence s : tokenHash.get(key).getOccurenceList()) {

                if (tagList.get(3).contains(key)) {
                    postingFileWriter.write(StandardCharsets.UTF_8.encode(fileName(s.getDocRef().getFile().getName()).replaceAll("\\W", "") + ' ' + (s.getFrequency() / (double) maxFreq.get(s.getDocRef())) * 1.5 + ' ' + tokenHash.get(key).getIdf() + ' ' + whereSeen.get(s.getDocRef().getFile().getName()) + '\n').array());
                    whereTheHellISeeTheTerm = postingFileWriter.getFilePointer();
                } else if (tagList.get(2).contains(key)) {
                    postingFileWriter.write(StandardCharsets.UTF_8.encode(fileName(s.getDocRef().getFile().getName()).replaceAll("\\W", "") + ' ' + (s.getFrequency() / (double) maxFreq.get(s.getDocRef())) * 1.5 + ' ' + tokenHash.get(key).getIdf() + ' ' + whereSeen.get(s.getDocRef().getFile().getName()) + '\n').array());
                    whereTheHellISeeTheTerm = postingFileWriter.getFilePointer();
                } else if (tagList.get(1).contains(key)) {
                    postingFileWriter.write(StandardCharsets.UTF_8.encode(fileName(s.getDocRef().getFile().getName()).replaceAll("\\W", "") + ' ' + (s.getFrequency() / (double) maxFreq.get(s.getDocRef())) * 1.5 + ' ' + tokenHash.get(key).getIdf() + ' ' + whereSeen.get(s.getDocRef().getFile().getName()) + '\n').array());
                    whereTheHellISeeTheTerm = postingFileWriter.getFilePointer();
                } else {
                    postingFileWriter.write(StandardCharsets.UTF_8.encode(fileName(s.getDocRef().getFile().getName()).replaceAll("\\W", "") + ' ' + (s.getFrequency() / (double) maxFreq.get(s.getDocRef())) + ' ' + tokenHash.get(key).getIdf() + ' ' + whereSeen.get(s.getDocRef().getFile().getName()) + '\n').array());
                    whereTheHellISeeTheTerm = postingFileWriter.getFilePointer();
                }

            }

        }
        System.out.println("DONE WRITE POSTING & VOC FILE! ");
        postingFileWriter.close();
        vocabularyWriter.close();
        System.out.println("DONE INDEXING!");
    }

}
