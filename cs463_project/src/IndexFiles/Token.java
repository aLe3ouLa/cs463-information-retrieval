package IndexFiles;

import java.util.ArrayList;
import java.util.List;

public class Token {
    
    private double idf; /* Inverse document frequency */
    private List<TokenOccurence> occurenceList; /* List of the Documents this token occurs*/

    public Token(){
        idf = 0;
        occurenceList = new ArrayList<>();
    }

    /**
     * @return the idf
     */
    public double getIdf() {
        return idf;
    }

    /**
     * @param idf the idf to set
     */
    public void setIdf(double idf) {
        this.idf = idf;
    }

    /**
     * @return the occurenceList
     */
    public List<TokenOccurence> getOccurenceList() {
        return occurenceList;
    }

    /**
     * @param occurenceList the occurenceList to set
     */
    public void setOccurenceList(TokenOccurence occurenceList) {
        this.occurenceList.add(occurenceList);
    }
}
