/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IndexFiles;

import java.io.BufferedReader;
import java.io.File;

/**
 *
 * @author Alexandra
 */
public class FileDocument {
    private File file;
   // private BufferedReader reader;

    public FileDocument(File file){
        this.file = file;
    }
    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @param file the file to set
     */
    public void setFile(File file) {
        this.file = file;
    }

   
}
