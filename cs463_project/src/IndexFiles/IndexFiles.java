package IndexFiles;

import gr.uoc.csd.hy463.TopicsReader;
import gr.uoc.csd.hy463.Topic;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringTokenizer;

public class IndexFiles {

    public static void createHashStopWords(String filename, HashSet<String> hs) throws UnsupportedEncodingException, FileNotFoundException, IOException {

        BufferedReader stopwords = null;
        StringTokenizer tokenizer = null;
        String delimiter = "\t\n\r\f ";
        String line = null, currentToken = null;
        stopwords = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"));
        while ((line = stopwords.readLine()) != null) {
            tokenizer = new StringTokenizer(line, delimiter);
            while (tokenizer.hasMoreTokens()) {
                currentToken = tokenizer.nextToken();
                hs.add(currentToken);
            }
        }

    }

    public static String removePunctuation(String currentToken) {
        //String newToken = "";

        String newToken = currentToken.replaceAll("[{}+.^!%~`!$^|=@*$:,\\?\\[\\]()''\"]", "");
        // String newToken = currentToken.replaceAll("^\\s*\\p{Punct}+\\s*$", "");
        /*for (int i = 0; i < currentToken.length(); i++) {
         char c = currentToken.charAt(i);
         if (Character.isLetterOrDigit(c)) {
         newToken += currentToken.charAt(i);

         }
         }*/

        //System.out.println("Current Token is: " + currentToken + " New token is: "+newToken);
        return newToken;
    }

    public static void createHashGR(HashSet<String> hs) throws UnsupportedEncodingException, FileNotFoundException, IOException {

        BufferedReader stopwords = null;
        StringTokenizer tokenizer = null;
        String delimiter = "\t\n\r\f ";
        String line = null, currentToken = null;
        stopwords = new BufferedReader(new InputStreamReader(new FileInputStream("./stopwordsGr.txt"), "UTF-8"));
        while ((line = stopwords.readLine()) != null) {
            tokenizer = new StringTokenizer(line, delimiter);
            while (tokenizer.hasMoreTokens()) {
                currentToken = tokenizer.nextToken();
                hs.add(currentToken);
            }
        }

    }

    public static boolean isInStopWordsGR(HashSet<String> hs, String str) throws UnsupportedEncodingException, FileNotFoundException, IOException {
        return hs.contains(str);

    }

    public static void createHashEN(HashSet<String> hs) throws UnsupportedEncodingException, FileNotFoundException, IOException {

        BufferedReader stopwords = null;
        StringTokenizer tokenizer = null;
        String delimiter = "\t\n\r\f ";
        String line = null, currentToken = null;
        stopwords = new BufferedReader(new InputStreamReader(new FileInputStream("./stopwordsEn.txt"), "UTF-8"));
        while ((line = stopwords.readLine()) != null) {
            tokenizer = new StringTokenizer(line, delimiter);
            while (tokenizer.hasMoreTokens()) {
                currentToken = tokenizer.nextToken();
                hs.add(currentToken);
            }
        }

    }

    public static boolean isInStopWordsEN(HashSet<String> hs, String str) {
        return hs.contains(str);

    }

    public static void listFilesForFolder(File folder, HashMap<String, File> fileList) {
        if (folder != null) {
            for (File fileEntry : folder.listFiles()) {
                if (fileEntry.isDirectory()) {
                    listFilesForFolder(fileEntry, fileList);
                } else {
                    if (fileList.containsKey(fileEntry.getName())){
                        
                    }else{
                     //   System.out.println(fileEntry.getName());
                        fileList.put(fileEntry.getName(), fileEntry);
                    }
                    //fileList.add(fileEntry);
                    
                }
            }
            
        }
    }

    public static ArrayList<Topic> readTopicsFile() throws Exception {
        ArrayList<Topic> topics = TopicsReader.readTopics("./topics.xml");
        return topics;
    }

     public static String fileName(String file) {
        file = file.replace(".nxml", "");
        return file;
    }
}
