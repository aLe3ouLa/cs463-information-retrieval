package IndexFiles;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;

public class FrontEnd extends javax.swing.JFrame {

    static String file;
    static MainClass model;
    Container pane;

    public FrontEnd() throws IOException {
        super("Ευρετηρίαση εγγράφων");

        pane = this.getContentPane();

        this.setLayout(new BorderLayout());
        this.setSize(600, 400);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        pane.setBackground(new Color(178, 223, 219));

        JPanel textsLayout = new JPanel();
        textsLayout.setLayout(new BoxLayout(textsLayout, BoxLayout.PAGE_AXIS));

        String Title = "ΕΥΡΕΤΗΡΙΑΣΗ ΕΓΓΡΑΦΩΝ.\n ";
        String text = "Επιλέξτε έναν φάκελο για να αρχίσει η διαδικασία της ευρετηρίασης.\n ";

        JTextArea textAreaTitle = new JTextArea();
        JTextArea textAreaText = new JTextArea();
        Font fontTitle = new Font("Calibri", Font.BOLD, 18);
        Font fontText = new Font("Calibri", Font.BOLD, 14);

        textAreaTitle.setOpaque(false);
        textAreaTitle.setForeground(Color.BLACK);
        textAreaTitle.setWrapStyleWord(true);
        textAreaTitle.setText(Title);
        textAreaTitle.setFont(fontTitle);

        textAreaText.setOpaque(false);
        textAreaText.setForeground(Color.BLACK);
        textAreaText.setWrapStyleWord(true);
        textAreaText.setText(text);
        textAreaText.setFont(fontText);

        textsLayout.add(textAreaTitle);
        textsLayout.add(textAreaText);

        pane.add(textsLayout, BorderLayout.NORTH);

        JPanel buttonLayout = new JPanel();
        buttonLayout.setLayout(new BoxLayout(buttonLayout, BoxLayout.PAGE_AXIS));

        JButton chooseFile = new JButton("Διαλέξτε φάκελο");
        chooseFile.setBounds(20, 30, 50, 30);
        chooseFile.setFont(fontText);
        chooseFile.setMargin(new Insets(20, 20, 20, 20));
        chooseFile.addActionListener(new openDialogueToChooseFile());
        buttonLayout.add(chooseFile);
        buttonLayout.setBackground(new Color(178, 223, 219));

        
        pane.add(buttonLayout, BorderLayout.CENTER);

        this.setVisible(true);
    }

    private static class openDialogueToChooseFile implements ActionListener {

        JFileChooser chooser = new JFileChooser();
        JButton b;

        public openDialogueToChooseFile() {
        }

        @Override
        public void actionPerformed(ActionEvent e) {

            Object source = e.getSource();
            b = (JButton) source;
            b.setVisible(false);
            if (source instanceof JButton) {
                Window w = findWindow((Component) source);
                if (w instanceof FrontEnd) {
                    chooser.setCurrentDirectory(new java.io.File("./MedicalCollection"));
                    chooser.setDialogTitle("Choose file: ");
                    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                        System.out.println("getSelectedFile() : " + chooser.getSelectedFile());
                        file = chooser.getSelectedFile().toString();

                        try {
                            model = new MainClass(file);
                            w.dispatchEvent(new WindowEvent(w, WindowEvent.WINDOW_CLOSING));
                        } catch (IOException ex) {
                            Logger.getLogger(FrontEnd.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    } else {
                        System.out.println("No Selection ");
                        chooser.cancelSelection();
                        b.setVisible(true);
                    }

                }

            }

        }

        public static Window findWindow(Component c) {
            //System.out.println(c.getClass().getName());
            if (c instanceof Window) {
                return (Window) c;
            } else if (c instanceof JPopupMenu) {
                JPopupMenu pop = (JPopupMenu) c;
                return findWindow(pop.getInvoker());
            } else {
                Container parent = c.getParent();
                return parent == null ? null : findWindow(parent);
            }
        }
    }
}
