package IndexFiles;

public class TokenOccurence {
    private int frequency; /* How many times the tokens occurs in the document  */
    private DocumentRef docRef; /* Reference to the document */


    public TokenOccurence(int frequency, DocumentRef docRef ){
        this.frequency = frequency;
        this.docRef = docRef;
    }

    /**
     * @return the frequency
     */
    public int getFrequency() {
        return frequency;
    }

    /**
     * @param frequency the frequency to set
     */
    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    /**
     * @return the docRef
     */
    public DocumentRef getDocRef() {
        return docRef;
    }

    /**
     * @param docRef the docRef to set
     */
    public void setDocRef(DocumentRef docRef) {
        this.docRef = docRef;
    }

}
