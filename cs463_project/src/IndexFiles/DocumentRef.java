package IndexFiles;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DocumentRef {
    private File file;
    private double fileNormal;
    private HashMap <String, List<Integer>> pos;
    
    public DocumentRef(FileDocument doc){
        this.file = doc.getFile();
        this.fileNormal = 0;
    }

    public DocumentRef(File file, double fileNormal){
        this.file = file;
        this.fileNormal = fileNormal;
    }
    /**
     * @return the file
     */
    public File getFile() {
        return file;
    }

    /**
     * @return the fileNormal
     */
    public double getFileNormal() {
        return fileNormal;
    }

    /**
     * @param fileNormal the fileNormal to set
     */
    public void setFileNormal(double fileNormal) {
        this.fileNormal = fileNormal;
    }

    /**
     * @return the pos
     */
    public HashMap <String, List<Integer>> getPos() {
        return pos;
    }

    /**
     * @param pos the pos to set
     */
    public void setPos(HashMap <String, List<Integer>> pos) {
        this.pos = pos;
    }
    
     public void addPos(String tag, Integer pos) {
        if (this.pos.containsKey(tag)){
            this.pos.get(tag).add(pos);
        }else{
            List<Integer> i = new ArrayList<>();
            i.add(pos);
            this.pos.put(tag, i);
        }
        
    }
}
